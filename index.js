const fs = require('fs');
const cheerio = require('cheerio');
const axios = require('axios');

var jsonObject = []
const base_url = "http://www.pocketyoga.com/"


async function searchPage(index) {
    responseMain = await axios.get(`http://www.pocketyoga.com/Pose?cat=1&sub=1&dif=1&pg=${index}`)
    let $Home = cheerio.load(responseMain.data);
    $Home('.posesDisplay ul li').each(async function(i, elem) {
        new_url = base_url + $Home(this).children().attr('href').substring(2);
        responsePage = await axios.get(new_url)
        let $Pose = cheerio.load(responsePage.data);
        json = {
            id : $Home(this).children().attr('href').substring(7),
            name : $Pose('#mainSection > section > section > section.poseDescription > ul:nth-child(1) > li:nth-child(1) > h3').text(),
            category : $Pose('#mainSection > section > section > section.poseDescription > ul:nth-child(3) > li:nth-child(1) > span').text(),
            difficulty : $Pose(`#mainSection > section > section > section.poseDescription > ul:nth-child(3) > li:nth-child(2) > span`).text(),
            image: $Pose('.poseImages img').attr('src').substring(2),
            url: $Home(this).children().attr('href').substring(2),
            description: $Pose('#mainSection > section > section > section.poseDescription > ul:nth-child(4) > li > p').text(),
            benefits: $Pose('#mainSection > section > section > section.poseDescription > ul:nth-child(5) > li > p').text(),
            previous_poses: [],
            next_poses: [],
        };
        $Pose('#mainSection > section > section > section.poseImages > ul:nth-child(3) > li').each(function(i, elem) {
            json.previous_poses.push($Pose(this).children().attr('href').substring(7))
        })
        $Pose('#mainSection > section > section > section.poseImages > ul:nth-child(5) > li').each(function(i, elem) {
            json.next_poses.push($Pose(this).children().attr('href').substring(7))
        })

        jsonObject.push(json)
    })
    console.log(`Page: ${index}`)
}

// For each poses list pagination
async function main() {
    for (let index = 1; index < 8; index++) {
        await searchPage(index)
            .catch((err)=>{
                console.log(err)
            })
    }
    fs.writeFile('pose.json', JSON.stringify(jsonObject, null, 4), 'utf8', ()=>{
        console.log("finish")
    });
}


main()
.catch((err)=>{
    console.log(err)
})